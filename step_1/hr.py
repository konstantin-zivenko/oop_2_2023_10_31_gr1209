
from abc import ABC, abstractmethod


class PayrollSystem:
    def calculate_payroll(self, employees):
        print("Calculating Payrol")
        print("==================")
        for employee in employees:
            print(f"Payroll for: {employee.id} - {employee.name}")
            print(f"Check amount: {employee.calculate_payroll()}")
            print("")


class Employee(ABC):
    def __init__(self, id, name):
        self.id = id
        self.name = name

    @abstractmethod
    def calculate_payroll(self):
        pass


class SalaryEmployee(Employee):
    def __init__(self, id, name, weekly_salary) -> None:
        super().__init__(id, name)
        self.weekly_salary = weekly_salary
    
    def calculate_payroll(self):
        return self.weekly_salary


class HourlyEmployee(Employee):
    def __init__(self, id, name, hours_worked, hour_rate) -> None:
        super().__init__(id, name)
        self.hours_worked = hours_worked
        self.hour_rate = hour_rate

    def calculate_payroll(self):
        return self.hour_rate * self.hours_worked

    
class CommissionEmployee(SalaryEmployee):
    def __init__(self, id, name, weekly_slary, commission) -> None:
        super().__init__(id, name, weekly_slary)
        self.commission = commission
    
    def calculate_payroll(self):
        fixed = super().calculate_payroll()
        return fixed + self.commission
